
# Editoria Vanilla

The following is a brief guide following the discusion on [this Discourse post](https://discourse.coko.foundation/t/hosting-editoria-as-a-service/25/20)
This guide has been written for Ubuntu 18.04

This application is being developed by the [Coko Foundation](https://coko.foundation/). For more information, visit the project's [website](https://editoria.pub/) or our [chat channel](https://mattermost.coko.foundation/coko/channels/editoria).  
For the editor that Editoria uses, see its related project [Wax](https://gitlab.coko.foundation/wax/wax).  
For an updated RoadMap please see the ReadMe at [https://gitlab.coko.foundation/editoria/editoria](https://gitlab.coko.foundation/editoria/editoria)

## Dependencies
* Webserver (Nginx/Apache2)
* Postgresql - Database
* Docker
* Docker Compose
* Node  >= 8.3
* Yarn
* build-essential

## Getting started

Get your copy of the repository.  
```sh
git clone https://gitlab.coko.foundation/editoria/editoria-vanilla.git
cd editoria-vanilla
```
## Setup the database
```sql
create database editoria;
create user editoria_user with password 'SuperSecretButStrongPassword';
grant all privileges on database editoria to editoria_user;
```
You will also need the pgcrypto extension for the database. Here's how to add it:
```sql
\c editoria
create extension pgcrypto;
```
## Setup the webserver (nginx)
The following is a short nginx config file
```nginx
server {
    listen 80; listen [::]:80;
    server_name _;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    server_name _;

    ssl_certificate /etc/letsencrypt/live/_/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/_/privkey.pem;
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;

    ssl_protocols TLSv1.2;
    ssl_ciphers EECDH+AESGCM:EECDH+AES;
    ssl_ecdh_curve secp384r1;
    ssl_prefer_server_ciphers on;

    ssl_stapling on;
    ssl_stapling_verify on;

    add_header Strict-Transport-Security "max-age=15768000; includeSubdomains; preload";
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;

    location / {
        proxy_pass http://127.0.0.1:3050/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $http_host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect http://127.0.0.1:3050/ https://_;
    }
}
```
Please replace the underscore with the domain you are going to serve it under. If you do not need HTTPS (Highly Discouraged!!), you can use the following config:
```nginx
```nginx
server {
    listen 80; listen [::]:80;
    server_name _;

    location / {
        proxy_pass http://127.0.0.1:3050/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $http_host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect http://127.0.0.1:3050/ http://_;
    }
}
```
Again, you would need to replace the underscore with your domain
## Configuring Editoria
Since this guide regards the full deployment for Editoria, please copy the template below and edit it to your setup. 

There are 2 files you need to edit (**both under the Editoria source code**):

`config/production.env`
```bash
export PUBSWEET_SECRET='enter random strings of characters'

export POSTGRES_USER='editoria_user'
export POSTGRES_PASSWORD='SuperSecretButStrongPassword'
export POSTGRES_HOST='127.0.0.1'
export POSTGRES_DB='editoria'
export POSTGRES_PORT='5432'

export MAILER_USER='USERNAME'
export MAILER_PASSWORD='PASSWORD'
export MAILER_SENDER='EMAIL'
export MAILER_HOSTNAME='HOSTNAME'

export PASSWORD_RESET_URL='your-editoria-domain/password-reset'
export PASSWORD_RESET_SENDER='robot@your-editoria-domain'

#you do not need to edit the following
export LANGUAGE_PORT='8090'
export LANGUAGE_ENDPOINT='http://localhost'
export SERVER_PORT='3050'
export NODE_ENV='production'
```
The second one is `config/local-production.js`
```js
module.exports = {
  'pubsweet-server': {
    secret:
      'Same value as in the production.env',
    db: {
      port: 5432,
      database: 'editoria',
      user: 'editoria_user',
      password: 'SuperSecretButStrongPassword',
    },
  },
  'pubsweet-client': {
    converter: 'ucp',
  },
  mailer: {
    transport: {
      auth: {
        user: 'USERNAME',
        pass: 'PASSWORD',
      },
    },
  },
}
```
Now that you have configured Editoria, we're ready to install it. 
First, import the values from `config/production.env` into your session. You can do that by typing the following
```bash
export config/production.env
```
## DOCKER and POSTGRESQL
Editoria has a couple of dockerized dependencies, which should talk to our database at the same time that Editoria is running. To enable such a feature, you must first enable your Postgresql server the ability to listen on network interfaces. You do this by going to `/etc/postgresql/<VERSION-NUMBER>/main/postgresql.conf` and setting listen_address to the following value: `listen_addresses = '*'`

Now postgres can talk to the world. However we need to specify which interfaces it can listen to. To do that we go to `/etc/postgresql/<VERSION-NUMBER>/main/pg_hba.conf` and right above the IPv6 local connections we add this line:
`host    editoria       editoria_user  172.18.0.0/16           md5`
*Note:* In case you're wondering why we added `172.18.0.0/16` in there, that's Docker's network interfaces. We set this up this way so that if Docker containers have the right credentials, they can log in to the database, but people outside your network cannot log in there directly! (This doesn't make you bulletproof, but it makes you more secure)

There's one more thing you need to do to make docker and postgresql work together. Restart the database server. On systemd based operating systems, this is done by typing 
```bash
systemctl restart postgresql
```
## Docker
In order to take Editoria online, you need to configure it's dependencies, which all run on Docker. Please copy and edit the following file to `docker-compose.yml`: 
```docker
version: '3'

services:
  language-tool:
    image: silviof/docker-languagetool
    restart: always
    ports:
      - ${LANGUAGE_PORT}:8010
  job-xsweet:
    image: pubsweet/job-xsweet
    build:
      context: https://gitlab.coko.foundation/pubsweet/pubsweet.git#xsweet-docker
      dockerfile: components/server/job-xsweet/Dockerfile
      args:
        WAIT_SERVICE_PORT: REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}
    restart: always
    container_name: job-xsweet
    hostname: job-xsweet
    environment:
      - DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}/${POSTGRES_DB}
  job-epubcheck:
    image: editoria/job-epubcheck
    build:
      context: https://gitlab.coko.foundation/editoria/editoria.git
      dockerfile: packages/job-epubcheck/Dockerfile
      args:
        WAIT_SERVICE_PORT: REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}
    restart: always
    container_name: job-epubcheck
    hostname: job-epubcheck
    environment:
      - DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}/${POSTGRES_DB}
    volumes:
     - ./uploads:/uploads/
  job-pandoc:
    image: editoria/job-pandoc
    build:
      context: https://gitlab.coko.foundation/editoria/editoria.git
      dockerfile: packages/job-pandoc/Dockerfile
      args:
        WAIT_SERVICE_PORT: REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}
    restart: always
    container_name: job-pandoc
    hostname: job-pandoc
    environment:
      - DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}/${POSTGRES_DB}
    volumes:
     - ./uploads:/uploads/
  job-pdf:
    image: editoria/job-pdf
    build:
      context: https://gitlab.coko.foundation/editoria/editoria.git
      dockerfile: packages/job-pdf/Dockerfile
      args:
        WAIT_SERVICE_PORT: REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}
    restart: always
    container_name: job-pdf
    hostname: job-pdf
    security_opt:
      - seccomp:seccomp.json
    environment:
      - DATABASE_URL=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@REPLACE-THIS-WITH-PUBLIC-IP-OF-POSTGRESQL-SERVER:${POSTGRES_PORT}/${POSTGRES_DB}
    volumes:
     - ./uploads:/uploads/

```

Now setup the database by running `yarn setupdb` on the Editoria source code.

If you want to have Editoria always running on this server, please copy, paste and edit the following 
```
[Unit]
Description=PubSweet Server

[Service]
ExecStart=/bin/bash -c '. "$0" && exec "$@"' /opt/editoria/config/production.env /opt/editoria/node_modules/.bin/pubsweet server
WorkingDirectory=/opt/editoria
Restart=always

[Install]
WantedBy=multi-user.target

```
Replace /opt/editoria/ with the location of Editoria. Save this file at 
`/etc/systemd/system/pubsweet.service`

```
[Unit]
Description=Editoria dependencies
After=pubsweet.service

[Service]
ExecStart=/bin/bash -c '. "$0" && exec "$@"' /opt/editoria/config/production.env /usr/local/bin/docker-compose up job-xsweet language-tool job-epubcheck job-pandoc job-pdf
WorkingDirectory=/opt/editoria
Restart=always

[Install]
WantedBy=multi-user.target
```
As with the previous file, you need to edit /opt/editoria with the absolute location of your Editoria source code. 
Save this file at `/etc/systemd/system/editoria-dependencies.service`

All you have to do now, is type the following
```bash
systemctl daemon-reload
systemctl enable pubsweet editoria-dependencies 
systemctl start pubsweet editoria-dependencies 
```
